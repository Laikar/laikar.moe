function copyToClipboard(text) {
  const listener = function(ev) {
    ev.preventDefault();
    ev.clipboardData.setData('text/plain', text);
  };
  document.addEventListener('copy', listener);
  document.execCommand('copy');
  document.removeEventListener('copy', listener);
}

function toggleCategory(categoryId){
  let category_toggle = document.getElementById(categoryId+'_toggle');
  let category_container = document.getElementById(categoryId);
  if (category_toggle.classList.contains("disabled")){
    category_toggle.classList.remove("disabled")
    category_container.classList.remove("disabled")
  }else{
    category_toggle.classList.add("disabled")
    category_container.classList.add("disabled")
  }


}