+++
title = "Laikar.moe"
template = "cv.html"
[extra]
experience = "Experiència"
education = "Formació"
projects_title = "Projectes"

[[extra.links]]
title = "Enllaç al email"
target = "mailto:david@laikar.moe"
icon = "email.svg"

[[extra.links]]
title = "Enllaç a Github"
target = "https://github.com/Laikar"
icon = "github.svg"

[[extra.links]]
title = "Enllaç a Gitlab"
target = "https://gitlab.com/Laikar"
icon = "gitlab.svg"

[[extra.links]]
title = "Enllaç a Instagram"
target = "https://www.instagram.com/laikar_/"
icon = "instagram.svg"

[[extra.links]]
title = "Currículum Vitae en PDF"
target = "/cv.pdf"
icon = "cv.svg"

[[extra.works]]
time = "Febrer 2023 - Abril 2023"
title = "Administrador de sistemes"
description = """\
Desplegament de la infraestructura cloud necessària per a diverses aplicacions mòbils.
"""

[[extra.works]]
time = "Novembre 2019 - Març 2020"
title = "Desenvolupador d'aplicacions"
description = """\
Desenvolupament d'aplicacions d'escriptori amb tecnologies .NET.
Programació de sistemes IoT amb Arduino.
"""

[[extra.works]]
time = "Novembre 2017 - Març 2018"
title = "Tècnic Informàtic"
description = """\
Desplegament i manteniment d'equips de xarxes i sistemes informàtics.
Preparació d'equips informàtics retirats per al seu reciclatge.
"""

[[extra.edu]]
time = "Setembre 2022 - Juny 2023"
type = "CFGS"
title = "Administració de sistemes i xarxes, especialització en Ciberseguretat"
place = "Institut Tecnològic de Barcelona"

[[extra.edu]]
time = "Setembre 2018 - Març 2020"
type = "CFGS"
title = "Desenvolupament d'Aplicacions Web"
place = "Institut Jaume Balmes"

[[extra.edu]]
time = "Setembre 2016 - Juny 2018"
type = "CFGM"
title = "Sistemes Microinformàtics i Xarxes"
place = "Institut Jaume Balmes"


[[extra.projects]]
title = "Ansible Home Cloud"
image = "/img/ahs.png"
technologies = [
    'Ansible',
    'Docker',
    'Python',
    'MariaDB',
    'PostgreSQL',
]
link = ""
description = """\
Playbook d'Ansible per desplegar un entorn cloud domèstic.
"""
+++
