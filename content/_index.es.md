+++
title = "Laikar.moe"
template = "cv.html"
[extra]
experience = "Experiencia"
education = "Formación"
projects_title = "Proyectos"

[[extra.links]]
title = "Email Link"
target = "mailto:david@laikar.moe"
icon = "email.svg"

[[extra.links]]
title = "Github Link"
target = "https://github.com/Laikar"
icon = "github.svg"

[[extra.links]]
title = "Gitlab Link"
target = "https://gitlab.com/Laikar"
icon = "gitlab.svg"

[[extra.links]]
title = "Instagram Link"
target = "https://www.instagram.com/laikar_/"
icon = "instagram.svg"

[[extra.links]]
title = "Curriculum Vitae en PDF"
target = "/cv.pdf"
icon = "cv.svg"

[[extra.works]]
time = "Febrero 2023 - Abril 2023"
title = "Administrador de sistemas"
description = """\
Despliegue de la infraetructura cloud necesaria para varias aplicaciones mobiles
"""

[[extra.works]]
time = "Noviembre 2019 - Marzo 2020"
title = "Desarrollador de aplicaciónes"
description = """\
Desarrollo de aplicación de escritorio con tecnologías .NET
Programación de sistemas IOT con Arduino.
"""

[[extra.works]]
time = "Noviembre 2017 - Marzo 2018"
title = "Tecnico Informatico"
description = """\
Despliegue y mantenimiento de equipo de redes y sistemas informaticos.
Preparación de equipos informáticos decomisionados para su reciclaje.
"""

[[extra.edu]]
time = "Septiembre 2022 - Junio 2023"
type = "CFGS"
title = "Administración de sistemas y redes, especialización en Ciberseguridad"
place = "Instut Tecnologic de Barcelona"

[[extra.edu]]
time = "Septiembre 2018 - Marzo 2020"
type = "CFGS"
title = "Desarrollo de Aplicaciónes Web"
place = "Instut Jaume Balmes"

[[extra.edu]]
time = "Septiembre 2016 - Junio 2018"
type = "CFGM"
title = "Sistemas Microinformaticos Y Redes"
place = "Instut Jaume Balmes"


[[extra.projects]]
title = "Ansible Home Cloud"
image = "/img/ahs.png"
technologies = [
    'Ansible',
    'Docker',
    'Python',
    'MariaDB',
    'PostgreSQL',
]
link = ""
description = """\
Playbook de ansible para desplegar un entorno cloud casero
"""
+++
