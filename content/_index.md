+++
title = "Laikar.moe"
template = "cv.html"

[extra]
experience = "Experience"
education = "Education"
projects_title = "Projects"

[[extra.links]]
title = "Email Link"
target = "mailto:david@laikar.moe"
icon = "email.svg"

[[extra.links]]
title = "Github Link"
target = "https://github.com/Laikar"
icon = "github.svg"

[[extra.links]]
title = "Gitlab Link"
target = "https://gitlab.com/Laikar"
icon = "gitlab.svg"

[[extra.links]]
title = "Instagram Link"
target = "https://www.instagram.com/laikar_/"
icon = "instagram.svg"

[[extra.links]]
title = "Curriculum Vitae in PDF"
target = "/cv.pdf"
icon = "cv.svg"

[[extra.works]]
time = "February 2023 - April 2023"
title = "System administrator"
description = """\
Deployment of cloud server infraestructure require by some inhouse mobile apps.
"""

[[extra.works]]
time = "November 2019 - March 2020"
title = "Software Developer"
description = """\
Development and manteinance of a .NET C# desktop app.
Development and manteinance of IOT arduino systems.
"""


[[extra.works]]
time = "November 2017 - March 2018"
title = "IT Technician"
description = """\
Deployment and manteiance of network and computer systems equipment.
Preparation of decommissioned computer equipment for recycling.
"""

[[extra.edu]]
time = "September 2022 - June 2023"
type = "Vocational Training"
title = "Systems and Network Administration, specializing in Cybersecurity"
place = "Barcelona Institute of Technology"

[[extra.edu]]
time = "September 2018 - March 2020"
type = "Vocational Training"
title = "Web Application Development"
place = "Jaume Balmes Institute"

[[extra.edu]]
time = "September 2016 - June 2018"
type = "Vocational Training"
title = "Microcomputer Systems and Networks"
place = "Jaume Balmes Institute"


[[extra.projects]]
title = "Ansible Home Cloud"
image = "/img/ahs.png"
technologies = [
    'Ansible',
    'Docker',
    'Python',
    'MariaDB',
    'PostgreSQL',
]
link = ""
description = """\
Ansible playbook to deploy a home cloud environment.
"""
+++