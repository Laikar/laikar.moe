# Laikar.moe
This is the source code for [laikar.moe](https://laikar.moe "laikar.moe").

It is build using [zola](https://www.getzola.org/)

# Building
Clone the repo, and build with zola
```bash
git clone https://gitlab.com/Laikar/laikar.moe web
cd web
zola build
```
All the final html, js and css files will be found in web/public

# Credit
- Brand/tech icons [simpleicons.org](https://simpleicons.org)
- Flag SVGs [flagicons.lipis.dev/](https://flagicons.lipis.dev/)
- cv icon [emka](https://icon-icons.com/es/icono/curriculum-vitae-negocio-cv-trabajo-trabajo-plan-de-estudios/175611)